const XHR = require('web/utils/xhr_async.js');
const Image = require('./image.js');

const GridUI = require('web/grid.ui/index.js');


module.exports = class {
  constructor(cfg, navigation) {


    let index = this.index = cfg.index;
    let page_size = this.page_size = cfg.page_size;
    let max_pages = this.max_pages = cfg.max_pages;
    let max_index = this.max_index = cfg.max_index;
    let images = this.images = [];

    this.navigation = navigation;

    let grid_element = this.gride = document.createElement('table');
    navigation.display.appendChild(grid_element);
    
    let grid_ui = this.grid_ui = new GridUI(2, window.innerWidth, 10, 300, grid_element);
//    document.body.appendChild(grid_ui.element);
    grid_ui.resize(window.innerWidth);

    let cur_in_row = this.cur_in_row = grid_ui.items_in_row;
    let this_class = this;
  }

  static async init(index, page_size, max_index, navigation) {
    try {
      let this_class = new module.exports({
        index: index,
        page_size: page_size,
        max_index: max_index
      }, navigation);
      var videos = document.getElementsByTagName("video");

      window.addEventListener('scroll', function(ev) {
        this_class.checkScroll()
      }, false);
      return this_class;
    } catch (e) {
      console.error(e);
      return undefined;
    }
     
  }

  listen_resize() {
    let this_class = this;

    this.resize_listener = async function(ev) {
      try {
        if (!this_class.resizing) {
          console.log(this_class);
          console.log("resizing", Date.now(), this_class.resizing);
          this_class.resizing = true;
          console.log("resizing state now", this_class.resizing);
          this_class.grid_ui.resize(window.innerWidth);
          let inrow = this_class.grid_ui.items_in_row;
          if (inrow != this_class.cur_in_row) {
            this_class.destroyed = true;
            await this_class.resize(inrow);
            this_class.cur_in_row = inrow;
          }
          this_class.checkScroll();

          this_class.resizing = false;
          console.log("resizing state now", this_class.resizing);
          if (this_class.resize_again) {
            console.log("resize again");
            this_class.resize_again = false;
            await this_class.resize_listener();
          }
          console.log("end");
        } else {
          this_class.resize_again = true;
        }
      } catch(e) {
        console.error(e.stack);
      } 
    };
    window.addEventListener('resize', this.resize_listener);

  }


  async resize(inrow) {
    try {
      console.log("resize");
      if (!inrow) inrow = this.cur_in_row; else this.cur_in_row = inrow;
      this.images.forEach(function(imag) {
        imag.destroy();
      });
      this.grid_ui.remove_all();
      this.images = [];
  //    document.body.appendChild(this.grid_ui.element);

      this.max_index = await XHR.get('/content-manager/gallery', {
        command: "max_index"
      });

      let page_size_navi = this.page_size;

      let max_index = await XHR.get('/content-manager/gallery', {
        command: "max_index"
      });

      await this.navigation.resize(page_size_navi, max_index);
      console.log("navigation resized");
      await this.load_media();
      console.log("media loaded");
    } catch (e) {
      console.error(e.stack);
    }
  }

  async load_media() {
    try {
      let min = Math.max(0, this.max_index - (this.index*this.page_size+this.page_size))+1;
      let max = this.max_index - this.index*this.page_size;
      if (this.index !== 0) {
     //   max += 1;
      };

      let media_items = await XHR.get('/content-manager/gallery', {
        command: "page",
        min: min,
        max: max
      });
      console.log(media_items);
      let srcs = [];
      for (let i = 0; i < media_items.length; i++) {
        let item = media_items[i];
        srcs.push(item.src);
      }

      for (var i = 0; i < media_items.length; i++) {
        var src = media_items[i];
        var image = await Image.init(src, this.grid_ui, this.images, this.navigation);

        if (image) {
          this.images.push(image);
          this.grid_ui.add(image.element);
        }
        this.grid_ui.resize(window.innerWidth);
      }

      for (var i = 0; i < this.images.length; i++) {
        let image = this.images[i];

        if (image.vid) {
          if (isMobile()) {
            image.vid.currentTime = "0.1";
          } else {
            image.vid.play();
          }
        }
      }
      
      hide_loading_overlay()
    } catch (e) {
      console.error(e.stack);
    }
  }
  
  checkScroll() {
    if (!isMobile()) {
      const fraction = 0.2;
      for(var i = 0; i < this.images.length; i++) {
        var image = this.images[i];
        if (image.vid) {
          var video = image.vid;
          var vpo = video.getBoundingClientRect();
          var x = vpo.left, y = vpo.top, r = vpo.right, b = vpo.bottom,
          w = video.offsetWidth, h = video.offsetHeight,
          visibleX, visibleY, visible;

          visibleX = Math.max(0, Math.min(w, window.innerWidth - x, r));
          visibleY = Math.max(0, Math.min(h, window.innerHeight - y, b));

          visible = visibleX * visibleY / (w * h);

          if (visible > fraction) {
            video.play();
          } else {
            video.pause();
          }
        }
      }
    } 
  }
  
  destroy() {
    this.delete_selection_button.removeEventListener('click', this.delete_listener);
    window.removeEventListener('resize', this.resize_listener);

    this.images.forEach(function(imag) {
      imag.destroy();
    });
    this.grid_ui.remove_all();
    this.images = [];
//    document.body.removeChild(this.grid_ui.element);
  }
}

    function isMobile() {
      return window.matchMedia("only screen and (max-width: 760px)").matches;
    }


