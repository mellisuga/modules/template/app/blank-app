
(async () => {
  try {
    const XHR = require('web/utils/xhr_async.js');
    const ContentNavigator = require('web/content_navigator/index.js');
    const MediaPage = require('./media_page.js');

    let max_index = await XHR.get('/content-manager/gallery', {
      command: "max_index"
    });

    let elem = document.getElementById("gallery");

    let navigator = await ContentNavigator.init({
      div: elem,
      max_index: max_index,
      page_size: 6
    }, async function(index, page_size, max_index, content_navigator) {
      let media_page = await MediaPage.init(
        index, page_size, max_index, content_navigator
      );
      await media_page.resize();
      return media_page; // TODO: remove max_pages argument as it is never used nor will be...
    });
    await navigator.get_page(0, navigator.page_size);
    console.log("test");

  } catch (e) {
    console.error(e);
  }
})();

