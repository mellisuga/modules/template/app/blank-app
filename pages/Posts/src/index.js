
'use strict'

const XHR = require('web/utils/xhr_async.js');

const ContentNavigator = require('web/content_navigator/index.js');
const Page = require('./page.js');

//const Post = require('./post.js');

require('./style.css');

(async function() {
  try {
    let div = document.body.querySelector('.content-block');
    console.log(div, "div");

    div.classList.add('posts');

    var post_list = div.querySelector(".post_list");

    let max_index = await XHR.get('/content-manager/posts', {
      command: "max_index"
    });




    let navigator = await ContentNavigator.init({
      page_size: 10,
      max_index: max_index,
      div: div
    }, async function(index, page_size, max_index, content_navigator) {
      console.log("navi_cb", content_navigator);
      if (index === 0) page_size -= 1;
      return await Page.init(
        index, page_size, max_index, content_navigator
      ); // TODO: remove max_pages argument as it is never used nor will be...
    });
    await navigator.select_page(0);
    
  } catch (e) {
    console.error(e.stack);
  }
})();
