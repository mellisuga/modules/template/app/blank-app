

'use strict'

const XHR = require('web/utils/xhr_async.js');
const Post = require('./post.js');

const Page = require('web/content_navigator/page.js');

module.exports = class {
  constructor(navigator, index) {
    this.navipage = navigator.pageByIndex(index);

    this.navigator = navigator;

  }

  static async init(index, page_size, max_index, navigator) {
    console.log("navi", navigator);
    let this_class = new module.exports(navigator, index);
    this_class.index = index;
    this_class.page_size = page_size;


    console.log(navigator);
    let post_list = this_class.post_list = navigator.display;


    this_class.resize();

    return this_class;
  }

  async resize() {
    try {
      this.post_list.innerHTML = "";

      let min = this.index*this.page_size;
      let max = this.index*this.page_size+this.page_size+1;

      console.log("this.index", this.index);
      console.log("this.page_size", this.page_size);

      let posts = await XHR.get('/content-manager/posts', {
        command: "page",
        min: min,
        max: max
      });

      for (let p = 0; p < posts.length; p++) {
        var post = new Post(posts[p], this);
        this.post_list.insertBefore(post.element, this.post_list.firstChild);
      }

      let max_index = await XHR.get('/content-manager/posts', {
        command: "max_index"
      });
      
      await this.navigator.resize(this.navigator.page_size, max_index);
    } catch (e) {
      console.error(e.stack);
    }
  }

  destroy() {

  }
}
