const fs = require("fs");
const path = require("path");

const Mellisuga = require("mellisuga");

(async function() {
  try {
    mellisuga = await Mellisuga.construct({
      host: '127.0.0.1',
      port: 9639,
      lang: "lt",
      app_path: __dirname,
/*      smtp: {
        gmail: 'smtps://qualphey%40gmail.com:'+pwds.main+'@smtp.gmail.com'
      },*/
      dev_mode: true, // false or remove this line in production
      disable_super: false, // true in production
      content_manager: {
 /*       context: {
          theme: "lsbaldai"
        },*/

      }
    });
  } catch(e) {
    console.log(e.stack);
  }
})()
